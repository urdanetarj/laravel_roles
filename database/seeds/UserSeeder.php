<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Ricardo Urdaneta',
            'email'=>'urdanetarj1994@gmail.com',
            'password'=>bcrypt('1234'),
        ])->assignRole('Admin');

        User::create([
            'name'=>'Pepito Lacra',
            'email'=>'consulta@gmail.com',
            'password'=>bcrypt('1234'),
        ])->assignRole('Consulta');
    }
}
