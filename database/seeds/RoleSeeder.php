<?php

use Illuminate\Database\Seeder;
use  Spatie\Permission\Models\Role;
use  Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $role_admin=Role::create(['name'=>'Admin']);
            $role_consulta=Role::create(['name'=>'Consulta']);

            Permission::create(['name'=>'admin.users.index'])->syncRoles([$role_admin,$role_consulta]);

            Permission::create(['name'=>'admin.users.show'])->syncRoles([$role_admin]);
            Permission::create(['name'=>'admin.users.edit'])->syncRoles([$role_admin]);
            Permission::create(['name'=>'admin.users.create'])->syncRoles([$role_admin]);
            Permission::create(['name'=>'admin.users.destroy'])->syncRoles([$role_admin]);



    }
}
